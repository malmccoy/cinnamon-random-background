#!/bin/bash
#My random wallpaper generator

printf 'Thanks for trying out my simple script! I hope it will lead to your next favorite desktop background. Please point me to a directory with pictures.' | fold -s

echo

read picDirectory

cd $picDirectory

coolPic=$(find ./ -type f -printf "%f\n" | shuf -n 1)

gsettings set  org.cinnamon.desktop.background picture-uri "file://${picDirectory}/${coolPic}"
